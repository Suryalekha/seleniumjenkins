package Utils;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class Reports {
	
	public static ExtentHtmlReporter html;
	public static ExtentReports extent;
	public ExtentTest test;
	
	public void reportSteps(String status,String data) {
		
		if (status.equalsIgnoreCase("Pass")) {
			test.pass(data);
		} else if (status.equalsIgnoreCase("Fail")) {
			test.fail(data);
		}else if (status.equalsIgnoreCase("Warning")) {
			test.warning(data);
		}	
	}
	//Before Class
	@BeforeClass
	public void beforeClass(){
		extent.attachReporter(html);
		test = extent.createTest("Tc001_LoginAndLogout", "Login to Leaftaps");
		test.assignAuthor("Suryalekha");
		test.assignCategory("smoke");
	}
	//Before Suite
	@BeforeSuite
		public void beforeSuite(){
			html= new ExtentHtmlReporter("./reports/result.html");
			html.setAppendExisting(true);
			extent=new ExtentReports();
		}
		//After Suite
		@AfterSuite
		public void afterSuite(){
			extent.flush();
		}
}
