package testcases;


	import org.openqa.selenium.WebElement;
	import org.testng.annotations.Test;

	import wdmethods.SeMethods;
	
	public class Tc002_CreateLead extends SeMethods {
		@Test
		public void createLead() {
		startApp("chrome", "http://leaftaps.com/opentaps");
		WebElement eleUsername = locateElement("id", "username");
		type(eleUsername, "DemoSalesManager");
		WebElement elePassword = locateElement("id", "password");
		type(elePassword, "crmsfa");
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin);
		WebElement eleCRMLink = locateElement("LinkText","CRM/SFA");
		click(eleCRMLink);
		WebElement eleLeadsLink = locateElement("LinkText","Leads");
		click(eleLeadsLink);
		WebElement eleCreateLeadLink = locateElement("LinkText","Create Lead");
		click(eleCreateLeadLink);
		
		//Create Lead Page
		WebElement eleCompName = locateElement("id", "createLeadForm_companyName");
		type(eleCompName, "Tata");
		WebElement eleFirstName = locateElement("id", "createLeadForm_firstName");
		type(eleFirstName, "Daksha");	
		WebElement eleLastName = locateElement("id", "createLeadForm_lastName");
		type(eleLastName, "Rupa");
		WebElement eleSource = locateElement("id", "createLeadForm_dataSourceId");
		selectDropDownUsingText(eleSource,"Conference");
		WebElement eleMarkCamp = locateElement("id", "createLeadForm_marketingCampaignId");
		selectDropDownUsingText(eleMarkCamp,"Automobile");
		WebElement eleFNameLoc = locateElement("id", "createLeadForm_firstNameLocal");
		type(eleFNameLoc, "Dk");
		WebElement eleLNameLoc = locateElement("id", "createLeadForm_lastNameLocal");
		type(eleLNameLoc, "RRR");
		WebElement eleSalu = locateElement("id", "createLeadForm_personalTitle");
		type(eleSalu, "Hi");
		WebElement eleTitle = locateElement("id", "createLeadForm_generalProfTitle");
		type(eleTitle, "Miss");
		WebElement eleAnnRev = locateElement("id", "createLeadForm_annualRevenue");
		type(eleAnnRev, "500000");
		WebElement eleDepName= locateElement("id", "createLeadForm_departmentName");
		type(eleDepName, "Treasury");
		WebElement eleCurr = locateElement("id", "createLeadForm_currencyUomId");
		selectDropDownUsingText(eleCurr,"INR - Indian Rupee");
		WebElement eleSicCode= locateElement("id", "createLeadForm_sicCode");
		type(eleSicCode, "GGGGGG");
		WebElement eleDesc= locateElement("id", "createLeadForm_description");
		type(eleDesc, "JJJJJJJJJJ");
		WebElement eleImpNote= locateElement("id", "createLeadForm_importantNote");
		type(eleImpNote, "QQQQQQQQQQQQ");
		WebElement eleTickSym= locateElement("id", "createLeadForm_tickerSymbol");
		type(eleTickSym, "aaaaaaaaaaa");
		WebElement eleNoEmp = locateElement("id", "createLeadForm_numberEmployees");
		type(eleNoEmp, "2000");
		WebElement eleInds = locateElement("id", "createLeadForm_industryEnumId");
		selectDropDownUsingText(eleInds,"Finance");
		WebElement eleOwner = locateElement("id", "createLeadForm_ownershipEnumId");
		selectDropDownUsingText(eleOwner,"Partnership");
		
		//Contact Info
		WebElement eleAreaCode= locateElement("id", "createLeadForm_primaryPhoneAreaCode");
		type(eleAreaCode, "044");
		WebElement elePhNo= locateElement("id", "createLeadForm_primaryPhoneNumber");
		type(elePhNo, "26565158");
		WebElement eleAskName= locateElement("id", "createLeadForm_primaryPhoneAskForName");
		type(eleAskName, "Abi");
		WebElement elePhExt= locateElement("id", "createLeadForm_primaryPhoneExtension");
		type(elePhExt, "13456");
		WebElement eleEmail= locateElement("id", "createLeadForm_primaryEmail");
		type(eleEmail, "DakshaRupa@gmail.com");
		WebElement eleURL= locateElement("id", "createLeadForm_primaryWebUrl");
		type(eleURL, "http://url.com/");
		
		WebElement eleToName= locateElement("id", "createLeadForm_generalToName");
		type(eleToName, "Dora");
		WebElement eleAttnName= locateElement("id", "createLeadForm_generalAttnName");
		type(eleAttnName, "kutty");
		WebElement eleAddr1= locateElement("id", "createLeadForm_generalAddress1");
		type(eleAddr1, "23,kannan street");
		WebElement eleAddr2= locateElement("id", "createLeadForm_generalAddress2");
		type(eleAddr2, "Avadi");
		WebElement eleCity= locateElement("id", "createLeadForm_generalCity");
		type(eleCity, "Chennai");
		WebElement eleState= locateElement("id", "createLeadForm_generalStateProvinceGeoId");
		type(eleState, "TAMILNADU");
		WebElement eleCtry= locateElement("id", "createLeadForm_generalCountryGeoId");
		type(eleCtry, "India");
		WebElement elePostCode= locateElement("id", "createLeadForm_generalPostalCode");
		type(elePostCode, "600129");
		
		WebElement eleSubmit = locateElement("class","smallSubmit");
		click(eleSubmit);
		WebElement eleFname= locateElement("id","viewLead_firstName_sp");
		verifyExactText(eleFname,"Daksha");
		closeBrowser();
		}	
	}

