package testcases;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import wdmethods.SeMethods;
//import testcases.WebDriverwait;

public class Tc003_EditLead extends SeMethods  {

	@Test
	public void editLead() {
		startApp("chrome", "http://leaftaps.com/opentaps");
		WebElement eleUsername = locateElement("id", "username");
		type(eleUsername, "DemoSalesManager");
		WebElement elePassword = locateElement("id", "password");
		type(elePassword, "crmsfa");
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin);
		WebElement eleCRMLink = locateElement("LinkText","CRM/SFA");
		click(eleCRMLink);
		WebElement eleLeadsLink = locateElement("LinkText","Leads");
		click(eleLeadsLink);
		WebElement eleFindLeadsLink = locateElement("LinkText","Find Leads");
		click(eleFindLeadsLink);
		WebElement eleFName = locateElement("XPath", "(//input[@name='firstName'])[3]");
		type(eleFName, "Daksha");
		WebElement eleFindLeadsBtn = locateElement("XPath","//button[text()='Find Leads']");
		click(eleFindLeadsBtn);
		//webdriver wait
		/*WebDriverWait wait = new WebDriverWait(driver,30);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//a[@class='linktext'])[4]")));*/
		
		
		WebElement eleFirstIDLink = locateElement("XPath","(//a[@class='linktext'])[4]");
		click(eleFirstIDLink);
		verifyTitle("View Lead | opentaps CRM");
		WebElement eleEditBtn = locateElement("XPath","(//a[@class='subMenuButton'])[3]");
		click(eleEditBtn);
		WebElement eleCmpName = locateElement("XPath","(//input[@name='companyName'])[2]");
		type(eleCmpName,"TestLeaf");
		WebElement eleUpdatBtn = locateElement("XPath","(//input[@class='smallSubmit'])[1]");
		click(eleUpdatBtn);
		WebElement eleCname = locateElement("id","viewLead_companyName_sp");
		verifyExactText(eleCname, "TestLeaf");
		System.out.println("Company name changed.");
	}
}
