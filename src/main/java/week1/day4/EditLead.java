package week1.day4;

import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class EditLead {

	public static void main(String[] args)  {
		
		ChromeDriver driver= new ChromeDriver();
		//Load URL
		driver.get("http:leaftaps.com/opentaps/");
		//maximize
		driver.manage().window().maximize();
		//Enter username
		driver.findElementById("username").sendKeys("DemoSalesManager");
		//password
		driver.findElementById("password").sendKeys("crmsfa");
		//Login
		driver.findElementByClassName("decorativeSubmit").click();
		//Click on CRMSFA link
		driver.findElementByLinkText("CRM/SFA").click();
		//Click on Leads link
		driver.findElementByLinkText("Leads").click();
		//Click on Find Leads link
		driver.findElementByLinkText("Find Leads").click();
		//Enter First name
		driver.findElementByXPath("(//input[@name='firstName'])[3]").sendKeys("Daksha");
		//Click on Find leads button
		driver.findElementByXPath("//button[text()='Find Leads']").click();
//		Thread.sleep(3000);
//		Click on first resulting lead id
		WebDriverWait wait = new WebDriverWait(driver,30);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//a[@class='linktext'])[4]")));
			
		driver.findElementByXPath("(//a[@class='linktext'])[4]").click();
		//Verify title of the page
		String titleOfPage = driver.getTitle();
		System.out.println("Title of page is"+" "+titleOfPage);
		String expectedTitle="View Lead | opentaps CRM";
		if(titleOfPage.equalsIgnoreCase(expectedTitle)) {
			System.out.println("Title verified");
		}
		//Click Edit
		driver.findElementByXPath("(//a[@class='subMenuButton'])[3]").click();
		//Change the company name
		driver.findElementByXPath("(//input[@name='companyName'])[2]").clear();
		driver.findElementByXPath("(//input[@name='companyName'])[2]").sendKeys("Selenium");
		//Click Update
		driver.findElementByXPath("(//input[@class='smallSubmit'])[1]").click();
		//Confirm the changed name appears
		String text = driver.findElementById("viewLead_companyName_sp").getText();
		System.out.println(text);
		if(text.contains("Selenium")) {
			System.out.println("Company name changed.");
		}
		else
		{
			System.out.println("Company name not changed");
		}
		driver.close();
	}
}
