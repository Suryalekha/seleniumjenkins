package week1.day4;

import org.openqa.selenium.Point;
import org.openqa.selenium.chrome.ChromeDriver;

public class Day4 {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		//Launch browser
		ChromeDriver driver= new ChromeDriver();
		//Load URL
		driver.get("http:leaftaps.com/opentaps/");
		//maximize
		driver.manage().window().maximize();
		//Enter username
		driver.findElementById("username").sendKeys("DemoSalesManager");
		//password
		driver.findElementById("password").sendKeys("crmsfa");
		//Login
		driver.findElementByClassName("decorativeSubmit").click();
		//getCurrentUrl
		String currentUrl = driver.getCurrentUrl();

		if(currentUrl.contains("login")) {
			System.out.println(currentUrl);
		}
			//getTitle
/*			driver.findElementByLinkText("CRM/SFA").click();
			String title = driver.getTitle();
			if (title.equals("My Home | opentaps CRM")) {
				System.out.println("Title matches");
			}
				//getText
				String text = driver.findElementByLinkText("Create Lead").getText();
				if (text.equals("Create Lead")) {
				System.out.println(text);
				}*/
		
				//getLocation
				Point location = driver.findElementByLinkText("CRM/SFA").getLocation();
				System.out.println(location);
				int x = driver.findElementByLinkText("CRM/SFA").getLocation().getX();
				int y = driver.findElementByLinkText("CRM/SFA").getLocation().getY();
				System.out.println(x+" "+y);
			}
			
	}

