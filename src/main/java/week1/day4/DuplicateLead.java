package week1.day4;

import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class DuplicateLead {
@Test(enabled=false)
	public void duplicateLead() throws InterruptedException {
		// TODO Auto-generated method stub
		ChromeDriver driver= new ChromeDriver();
		//Load URL
		driver.get("http:leaftaps.com/opentaps/");
		//maximize
		driver.manage().window().maximize();
		//Enter username
		driver.findElementById("username").sendKeys("DemoSalesManager");
		//password
		driver.findElementById("password").sendKeys("crmsfa");
		//Login
		driver.findElementByClassName("decorativeSubmit").click();
		//Click on CRMSFA link
		driver.findElementByLinkText("CRM/SFA").click();
		//Click on Leads link
		driver.findElementByLinkText("Leads").click();
		//Click on Find Leads link
		driver.findElementByLinkText("Find Leads").click();
		//Click on Email
		driver.findElementByXPath("//span[text()='Email']").click();
		driver.findElementByName("emailAddress").sendKeys("DakshaRupa@gmail.com");
		//Click on Find Leads button
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(3000);
		//Capture first name of First Resulting lead
		String text = driver.findElementByXPath("(//a[@class='linktext'])[6]").getText();	
		System.out.println(text);
		//Click on first resulting lead id
		driver.findElementByXPath("(//a[@class='linktext'])[4]").click();
		//Click Duplicate Lead
		driver.findElementByLinkText("Duplicate Lead").click();
		//Verify the title as 'Duplicate Lead'
		String text2 = driver.findElementById("sectionHeaderTitle_leads").getText();
		if(text2.equals("Duplicate Lead")) {
			System.out.println("Title is:"+text2);
		}
		else {
			System.out.println("Title is wrong");
		}
			//Click create lead
			driver.findElementByClassName("smallSubmit").click();
			if(text.equals("Daksha")) {
				System.out.println("Duplicated lead name is same as captured name");
			}
			else
			{
				System.out.println("Duplicated lead name is not same as captured name");
			}
			driver.close();
		}
	}

