package week1.day2;

import org.openqa.selenium.chrome.ChromeDriver;
//import org.testng.annotations.Test;


public class CreateLead {
//	@Test(invocationCount=2,invocationTimeOut=120000)

	public static void main(String[] args) {
	
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		//Launch browser
		ChromeDriver driver= new ChromeDriver();
		//Load URL
		driver.get("http:leaftaps.com/opentaps/");
		//maximize
		driver.manage().window().maximize();
		//Enter username
		driver.findElementById("username").sendKeys("DemoSalesManager");
		//password
		driver.findElementById("password").sendKeys("crmsfa");
		//Login
		driver.findElementByClassName("decorativeSubmit").click();
		//Click on CRMSFA link
		driver.findElementByLinkText("CRM/SFA").click();
		//Click on Leads link
		driver.findElementByLinkText("Leads").click();
		//Click on Create Lead link
		driver.findElementByLinkText("Create Lead").click();
		//Fill in form
		driver.findElementById("createLeadForm_companyName").sendKeys("Tata");
		driver.findElementById("createLeadForm_firstName").sendKeys("Daksha");
		driver.findElementById("createLeadForm_lastName").sendKeys("Rupa");

		driver.findElementById("createLeadForm_firstNameLocal").sendKeys("Dk");
		driver.findElementById("createLeadForm_lastNameLocal").sendKeys("RR");
		driver.findElementById("createLeadForm_personalTitle").sendKeys("Hi");
		driver.findElementById("createLeadForm_generalProfTitle").sendKeys("Miss");
		driver.findElementById("createLeadForm_annualRevenue").sendKeys("7000000");
		driver.findElementById("createLeadForm_departmentName").sendKeys("Treasury");
		driver.findElementById("createLeadForm_currencyUomId").sendKeys("INR - Indian Rupee");


		driver.findElementById("createLeadForm_sicCode").sendKeys("1000");
		driver.findElementById("createLeadForm_description").sendKeys("SSSSSSs");
		driver.findElementById("createLeadForm_importantNote").sendKeys("aaaaabbbbb");		
		driver.findElementById("createLeadForm_numberEmployees").sendKeys("3000");
		driver.findElementById("createLeadForm_tickerSymbol").sendKeys("symbol");

		driver.findElementById("createLeadForm_primaryPhoneAreaCode").sendKeys("044");
		driver.findElementById("createLeadForm_primaryPhoneNumber").sendKeys("26565158");
		driver.findElementById("createLeadForm_primaryPhoneAskForName").sendKeys("Abi");
		driver.findElementById("createLeadForm_primaryPhoneExtension").sendKeys("13456");
		driver.findElementById("createLeadForm_primaryEmail").sendKeys("DakshaRupa@gmail.com");		
		driver.findElementById("createLeadForm_primaryWebUrl").sendKeys("http://url.com/");

		driver.findElementById("createLeadForm_generalToName").sendKeys("Dora");
		driver.findElementById("createLeadForm_generalAttnName").sendKeys("kutty");
		driver.findElementById("createLeadForm_generalAddress1").sendKeys("23,kannan street");
		driver.findElementById("createLeadForm_generalAddress2").sendKeys("Avadi");
		driver.findElementById("createLeadForm_generalCity").sendKeys("Chennai");
		driver.findElementById("createLeadForm_generalStateProvinceGeoId").sendKeys("TAMILNADU");
		driver.findElementById("createLeadForm_generalCountryGeoId").sendKeys("India");
		driver.findElementById("createLeadForm_generalPostalCode").sendKeys("6000129");

		driver.findElementById("createLeadForm_dataSourceId").sendKeys("Conference");
		driver.findElementById("createLeadForm_industryEnumId").sendKeys("Finance");
		driver.findElementById("createLeadForm_ownershipEnumId").sendKeys("Partnership");
		driver.findElementById("createLeadForm_marketingCampaignId").sendKeys("Automobile");

		driver.findElementByClassName("smallSubmit").click();

		String text = driver.findElementById("viewLead_firstName_sp").getText();
		if(text.equals("Daksha")) {
			System.out.println("First name is verified.Lead created.");
		}
		else
		{
			System.out.println("Lead not created");
		}
		driver.findElementByLinkText("Logout").click();
		driver.close();
	}	
}


