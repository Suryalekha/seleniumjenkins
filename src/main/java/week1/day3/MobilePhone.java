package week1.day3;

public interface MobilePhone {
	
	public int dialcaller(int phonenum);
	public boolean sendSMS(int phonenum);
}
