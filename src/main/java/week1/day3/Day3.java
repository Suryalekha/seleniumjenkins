package week1.day3;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;



public class Day3 {

	@Test
	public void main() throws IOException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		//Launch browser
		ChromeDriver driver= new ChromeDriver();
		//Load URL
		driver.get("http:leaftaps.com/opentaps/");
		//maximize
		driver.manage().window().maximize();
		//Enter username
		driver.findElementById("username").sendKeys("DemoSalesManager");
		//password
		driver.findElementById("password").sendKeys("crmsfa");
		//Login
		driver.findElementByClassName("decorativeSubmit").click();
		//Click on CRMSFA link
		driver.findElementByLinkText("CRM/SFA").click();
		//Click on Leads link
		driver.findElementByLinkText("Leads").click();
		//Click on Create Lead link
		driver.findElementByLinkText("Create Lead").click();
		//Fill in form
		driver.findElementById("createLeadForm_companyName").sendKeys("Tata");
		driver.findElementById("createLeadForm_firstName").sendKeys("Daksha");
		driver.findElementById("createLeadForm_lastName").sendKeys("Rupa");

		//take screenshot 
		File src = driver.getScreenshotAs(OutputType.FILE);
		File dest = new File("./snaps/img.png");
		FileUtils.copyFile(src, dest);

		/*driver.findElementById("createLeadForm_lastNameLocal").sendKeys("RR");
		driver.findElementById("createLeadForm_departmentName").sendKeys("Treasury");
		driver.findElementById("createLeadForm_numberEmployees").sendKeys("2000");
		driver.findElementById("createLeadForm_tickerSymbol").sendKeys("symbol");
		driver.findElementById("createLeadForm_ownershipEnumId").sendKeys("Partnership");*/

		WebElement source=driver.findElementById("createLeadForm_dataSourceId");

		Select sc= new Select(source);
		sc.selectByVisibleText("Employee");


		WebElement byId = driver.findElementById("createLeadForm_marketingCampaignId");
		Select sc1= new Select(byId);
		sc1.selectByValue("CATRQ_CARNDRIVER");

		WebElement byId2 = driver.findElementById("createLeadForm_industryEnumId");
		Select sc2= new Select(byId2);
		List<WebElement> allOptions = sc2.getOptions();
		int i = allOptions.size();
		sc2.selectByIndex(i-1);

		System.out.println("Dropdown set using Select class");
		
		driver.findElementByClassName("smallSubmit").click();
		 
		driver.close();
		/*	for(int i=0; i<count; i++) {
			WebElement eachOption = allOptions.get(i);//allOptions.get(i).click();
			System.out.println(eachOption.getText());

			//syntax foreach
		for(WebElement eachOption: allOptions) {
		System.out.println(eachOption.getText());
	}
			
			driver.findElementByLinkText("Logout").click();
		}*/

	}

}
