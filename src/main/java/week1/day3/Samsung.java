package week1.day3;

public class Samsung implements MobilePhone{

	@Override
	public int dialcaller(int phonenum) {
		System.out.println("Dialled through mobile");
		return phonenum;		
	}

	@Override
	public boolean sendSMS(int phonenum) {
		System.out.println("Message sent through Samsung ");
		return false;
	}

}
