package week2.day5;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class zoomcar {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		ChromeDriver driver= new ChromeDriver();
		//Load URL
		driver.get("https://www.zoomcar.com/chennai/");
		//maximize
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		//click on start
		driver.findElementByLinkText("Start your wonderful journey").click();
		//select a pickup
		driver.findElementByXPath("(//div[@class='items'])[1]").click();
		//Click on Next
		driver.findElementByXPath("//button[text()='Next']").click();
		// Get the current date
		Date date = new Date();
		// Get only the date (and not month, year, time etc)
		DateFormat sdf = new SimpleDateFormat("dd"); 
		// Get today's date
		String today = sdf.format(date);
		// Convert to integer and add 1 to it
		int tomorrow = Integer.parseInt(today)+1;
		// Print tomorrow's date
		System.out.println(tomorrow);
		driver.findElementByXPath("//div[contains(text(),'"+tomorrow+"')]").click();
		driver.findElementByXPath("//button[text()='Next']").click();
		//Click on Done
		driver.findElementByXPath("//button[text()='Done']").click();
		//Get highest value
		List<WebElement> noOfItems = driver.findElementsByXPath("//div[@class='component-car-item']");
		System.out.println("Number of items displayed:"+noOfItems.size());
		List<WebElement> noOfPrice = driver.findElementsByXPath("//div[@class='price']");

		List<Integer> allPrice=new ArrayList<Integer>();

		for (WebElement eachPrice : noOfPrice) {
			String text = eachPrice.getText();
			String replaceAll = text.replaceAll("\\D", "");
			int price=Integer.parseInt(replaceAll);
			System.out.println(price);
			allPrice.add(price);		
		}

		System.out.println(allPrice);
		int maxval=Collections.max(allPrice);
		System.out.println("The highest price is:"+maxval);

		//Report Brand name
		String brandName = driver.findElementByXPath("//div[contains(text(),'"+maxval+"')]/preceding::h3[1]").getText();
		System.out.println("Brand name of highest value car is"+brandName);

		//Click Book Now button
		driver.findElementByXPath("//div[contains(text(),'"+maxval+"')]/following::button").click();
		driver.close();
	}

}