package week2.day1;

import java.util.List;

import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;


public class LearnFrame2 {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		ChromeDriver driver= new ChromeDriver();
		driver.get("https://jqueryui.com/");
		driver.manage().window().maximize();
		driver.findElementByLinkText("Draggable").click();
		WebElement frameElmnt = driver.findElementByClassName("demo-frame");
		driver.switchTo().frame(frameElmnt);
		String text = driver.findElementById("draggable").getText();
		System.out.println(text);
		Point location = driver.findElementById("draggable").getLocation();
		System.out.println(location);
		driver.switchTo().defaultContent();
		List<WebElement> frames = driver.findElementsByTagName("iframe");
		int size = frames.size();
		System.out.println(size);
		driver.close();
	}

}
