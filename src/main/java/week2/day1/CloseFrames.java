package week2.day1;

import org.openqa.selenium.chrome.ChromeDriver;

public class CloseFrames {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ChromeDriver driver= new ChromeDriver();
		driver.get("http://layout.jquery-dev.com/demos/iframe_local.html");
		driver.manage().window().maximize();
		driver.switchTo().frame("childIframe");
		driver.findElementByXPath("(//button[text()='Close Me'])[1]").click();
		System.out.println("Iframe West Button closed");
		driver.findElementByXPath("(//button[text()='Close Me'])[2]").click();
		System.out.println("Iframe East Button closed");
		driver.switchTo().defaultContent();
		driver.findElementByXPath("(//button[text()='Close Me'])[1]").click();
		System.out.println("West Button closed");
		driver.findElementByXPath("(//button[text()='Close Me'])[2]").click();
		System.out.println("East Button closed");
		driver.close();
	}

}
