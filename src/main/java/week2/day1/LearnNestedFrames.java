package week2.day1;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class LearnNestedFrames {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		ChromeDriver driver= new ChromeDriver();
		driver.get("https://www.w3schools.com/tags/tryit.asp?filename=tryhtml_frame_cols");
		driver.manage().window().maximize();
		Thread.sleep(2000);
		List<WebElement> frames = driver.findElementsByTagName("iframe");
		int size = frames.size();
		System.out.println("No. of frames"+size);

	}

}
