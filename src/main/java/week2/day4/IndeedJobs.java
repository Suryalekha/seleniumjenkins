package week2.day4;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;

public class IndeedJobs {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		RemoteWebDriver driver= new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.indeed.co.in/Fresher-jobs");
		driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
		List<WebElement> links = driver.findElementsByXPath("((//td[@id='resultsCol'])//a[@data-tn-element='jobTitle'])[1]");
//		int size = links.size();
		for (WebElement eachLink : links) {
			Actions builder= new Actions(driver);
			builder.keyDown(Keys.CONTROL).click(eachLink).perform();
				
			Set<String> allWindows = driver.getWindowHandles();
			List<String> list=new ArrayList<String>();
			list.addAll(allWindows);
			driver.switchTo().window(list.get(1));
			String title = driver.getTitle();
			System.out.println(title);
		}
	}

}
