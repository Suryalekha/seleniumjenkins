package week2.day6;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MyntraSite {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		RemoteWebDriver driver= new ChromeDriver();
		//Load URL
		driver.get("https://www.myntra.com/");
		//maximize
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		//In Searchbox type "Sunglasses"
		driver.findElementByClassName("desktop-searchBar").sendKeys("SunGlasses");
		driver.findElementByClassName("desktop-submit").click();
		String listOfItems = driver.findElementByClassName("horizontal-filters-sub").getText();
		System.out.println("List of all Products:"+listOfItems);


	/*	List<WebElement> allSunglasses = driver.findElementsByClassName("product-brand");
		for (WebElement eachGlass : allSunglasses) {
			System.out.println(eachGlass.getText());
		}*/

		/* JavascriptExecutor js = (JavascriptExecutor) driver;
		 js.executeScript("window.scrollBy(0,1000)");
		driver.findElementByXPath("(//div[@class='common-radioIndicator'])[3]").click();
		 WebElement disRad = driver.findElementByXPath("//input[@type='radio' and @value='20.0 TO 100.0']");
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.visibilityOf(disRad));
				(By.xpath("//input[@type='radio' and @value='20.0 TO 100.0']")));

//		WebElement disRad = driver.findElementByXPath("(//input[@type='radio']/following-sibling::div)[3]");
		disRad.click();*/


		List<WebElement> brandName = driver.findElementsByXPath("//span[text()='(30% OFF)']/preceding::h4[1][contains(text(),'Unisex')]/preceding-sibling::div");

		for (WebElement eachName : brandName) {
			System.out.println("Sunglasses Brand Name with 30% discount and Unisex:"+eachName.getText());

		}
		List<WebElement> brandPrice = driver.findElementsByXPath("//span[text()='(30% OFF)']/preceding::h4[1][contains(text(),'Unisex')]/preceding-sibling::div/following::span[2]");
		for (WebElement eachPrice : brandPrice) {
			System.out.println("Sunglasses Brand Price with 30% discount and Unisex:"+eachPrice.getText());
		}
		
		//Click on face shape and round
		driver.findElementByXPath("//h4[text()='Face Shape']").click();
		WebElement roundCheck = driver.findElementByXPath("//input[@type='checkbox' and @value='round']");
		Actions builder=new Actions(driver);
		boolean roundSelected = roundCheck.isSelected();
		System.out.println(roundSelected);
		
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.elementSelectionStateToBe(By.xpath("//input[@type='checkbox' and @value='round']"), roundSelected));
		builder.moveToElement(roundCheck).click().perform();
		
		Thread.sleep(3000);
		//Click on Type and Oval
		driver.findElementByXPath("//h4[text()='Type']").click();
		WebElement ovalCheck = driver.findElementByXPath("//input[@type='checkbox' and @value='oval sunglasses']");
		boolean OvalSelected = ovalCheck.isSelected();
		System.out.println(OvalSelected);
		wait.until(ExpectedConditions.elementSelectionStateToBe(ovalCheck, OvalSelected));
		builder.moveToElement(ovalCheck).click().perform();
		WebElement firstProduct = driver.findElementByXPath("(//div[@class='product-thumbShim']/following::a)[1]//img");
		wait.until(ExpectedConditions.elementToBeClickable(firstProduct));
		// clicking on first product
		builder.moveToElement(firstProduct).pause(1000).click().perform();
		Set<String> windowHandles = driver.getWindowHandles();
		List<String> ls=new ArrayList<>();
		ls.addAll(windowHandles);
		// switching to new window
		driver.switchTo().window(ls.get(1));
		// finding product name
		String productName = driver.findElementByTagName("h4").getText();
		System.out.println(productName);
	}

}
