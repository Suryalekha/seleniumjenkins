package week2.day2;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class MergeLead {
@Test
	public void main() throws InterruptedException {
		// TODO Auto-generated method stub
		ChromeDriver driver= new ChromeDriver();
		//Load URL
		driver.get("http:leaftaps.com/opentaps/");
		//maximize
		driver.manage().window().maximize();
		//Enter username
		driver.findElementById("username").sendKeys("DemoSalesManager");
		//password
		driver.findElementById("password").sendKeys("crmsfa");
		//Login
		driver.findElementByClassName("decorativeSubmit").click();
		//Click on CRMSFA link
		driver.findElementByLinkText("CRM/SFA").click();
		//Click on Leads link
		driver.findElementByLinkText("Leads").click();
		//Click Merge Leads
		driver.findElementByLinkText("Merge Leads").click();
		//Click on Icon near From Lead
		driver.findElementByXPath("(//img[@src='/images/fieldlookup.gif'])[1]").click();

		//Move to new window
		Set<String> allWindows = driver.getWindowHandles();
		List<String> list=new ArrayList<String>();
		list.addAll(allWindows);
		driver.switchTo().window(list.get(1));
		System.out.println(driver.getTitle());
		//Enter First Name
		driver.findElementByName("firstName").sendKeys("Gayatri");
		//Click Find Leads
		driver.findElementByXPath("(//button[@class='x-btn-text'])[1]").click();
		Thread.sleep(3000);
		String text = driver.findElementByXPath("(//a[@class='linktext'])[1]").getText();
		System.out.println(text);
		driver.findElementByXPath("(//a[@class='linktext'])[1]").click();
		//switch to primary window
		driver.switchTo().window(list.get(0));
		System.out.println(driver.getTitle());

		//Click on Icon near To Lead
		driver.findElementByXPath("(//img[@src='/images/fieldlookup.gif'])[2]").click();
		//Move to new window
		allWindows = driver.getWindowHandles();
		List<String> list1=new ArrayList<String>();
		list1.addAll(allWindows);
		driver.switchTo().window(list1.get(1));
		System.out.println(driver.getTitle());
		//Enter First Name
		driver.findElementByName("firstName").sendKeys("test");
		//Click Find Leads
		driver.findElementByXPath("(//button[@class='x-btn-text'])[1]").click();
		Thread.sleep(3000);
		String text1 = driver.findElementByXPath("(//a[@class='linktext'])[1]").getText();
		System.out.println(text1);
		driver.findElementByXPath("(//a[@class='linktext'])[1]").click();
		//switch to primary window
		driver.switchTo().window(list1.get(0));
		System.out.println(driver.getTitle());
		driver.findElementByLinkText("Merge").click();
		driver.switchTo().alert().accept();
		//Click on Find Leads link
		driver.findElementByLinkText("Find Leads").click();
		driver.findElementByXPath("(//input[@style='width: 212px;'])[1]").sendKeys(text);
		driver.findElementByXPath("(//button[@class='x-btn-text'])[7]").click();
		Thread.sleep(3000);
		String errMsg = driver.findElementByClassName("x-paging-info").getText();
		System.out.println(errMsg);
		driver.close();
	}

}
