package week2.day2;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.chrome.ChromeDriver;

public class LearnWindows {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ChromeDriver driver= new ChromeDriver();
		driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
		driver.manage().window().maximize();
		String currentUrl = driver.getCurrentUrl();
		System.out.println(currentUrl);
		System.out.println(driver.getTitle());
		driver.findElementByLinkText("Contact Us").click();
		Set<String> allWindows = driver.getWindowHandles();
		List<String> list=new ArrayList<String>();
		list.addAll(allWindows);
		driver.switchTo().window(list.get(1));
		System.out.println(driver.getTitle());
		driver.switchTo().window(list.get(0));
		driver.close();
	}

}
