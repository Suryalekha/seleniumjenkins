package week2.day2;

import java.util.ArrayList;
import java.util.List;

public class LearnList {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List <String> list= new ArrayList<String>();
		list.add("Babu");
		list.add("Sarath");
		list.add("Gopi");
		list.add("Srinidhi");
		list.add("Jhansi");
		System.out.println(list.size());
		System.out.println(list.get(1));
		System.out.println(list.contains("Gopi"));
		for (String each : list) {
			if(each.startsWith("S"))
			System.out.println(each);
		}
	}

}
