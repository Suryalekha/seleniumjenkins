package week2.day3;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class LearnList {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List <String> list= new ArrayList<String>();
		list.add("Kanna");
		list.add("Hari");
		list.add("Gopi");
		list.add("Srinidhi");
		list.add("Srinidhi");
		System.out.println(list.size());
		Set <String> set=new HashSet<String>();
		set.addAll(list);
		System.out.println(set.size());
		for (String eachName : set) {
			System.out.println(eachName);
		}
	}
				

}