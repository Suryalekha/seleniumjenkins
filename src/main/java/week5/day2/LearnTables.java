package week5.day2;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class LearnTables {

	public static void main(String[] args) {
	
		ChromeDriver driver= new ChromeDriver();
		//Load URL
		driver.get("http://www.leafground.com/pages/table.html");
		//maximize
		driver.manage().window().maximize();
		WebElement table = driver.findElementByXPath("//table");
		List<WebElement> rows = table.findElements(By.tagName("tr"));
		int rowcnt = rows.size();
		for (int row = 1; row <rowcnt ; row++) {
			List<WebElement> cols = rows.get(row).findElements(By.tagName("td"));
			System.out.println(cols.get(row).getText());
			if(cols.get(1).getText().equals("80%")) {
				cols.get(2).click();
			}
				
	}

}
}
