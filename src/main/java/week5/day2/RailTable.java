package week5.day2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class RailTable {

	public static void main(String[] args) {
		ChromeDriver driver= new ChromeDriver();
		//Load URL
		driver.get("https://erail.in/");
		//maximize
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		WebElement fromStn = driver.findElementById("txtStationFrom");
		fromStn.clear();
		fromStn.sendKeys("MAS",Keys.TAB);
		WebElement toStn = driver.findElementById("txtStationTo");
		toStn.clear();
		toStn.sendKeys("SBC",Keys.TAB);
		driver.findElementById("chkSelectDateOnly").click();
		
		WebElement table = driver.findElementByXPath("//table[@class='DataTable TrainList']");
		List<WebElement> rows = table.findElements(By.tagName("tr"));
		System.out.println(rows.size());
		List<String> list = new ArrayList<>();
		for (WebElement eachRow : rows) {
			List<WebElement> cols = eachRow.findElements(By.tagName("td"));
			String text = cols.get(1).getText();
			//System.out.println(text);
			list.add(text);
			Collections.sort(list);
		}
		
		System.out.println(list);
		
		driver.findElementByXPath("//a[text()='Train Name']").click();
		WebElement table2 = driver.findElementByXPath("//table[@class='DataTable TrainList']");
		List<WebElement> rows2 = table2.findElements(By.tagName("tr"));
		System.out.println(rows2.size());
		List<String> sortList = new ArrayList<>();
		for (WebElement eachRow : rows2) {
			List<WebElement> cols = eachRow.findElements(By.tagName("td"));
			String text = cols.get(1).getText();
			//System.out.println(text);
			sortList.add(text);
		}
		System.out.println(sortList);
		
		if (sortList.contains(list)) {
			System.out.println("matched");
		} else {
			System.out.println("not matched");
		}
		
	}

}
