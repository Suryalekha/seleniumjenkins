package string;

import org.openqa.selenium.support.ui.Select;

public class FindString {

	public static void main(String[] args) {
		/*String str= "hi pls find the in between characters";
		int findex = str.indexOf("s");
		System.out.println(findex);
		int lastIndex = str.lastIndexOf("s");
		String substring = str.substring(findex+1, lastIndex);
		System.out.println(substring.trim());
		System.out.println(substring.length());*/
//		public class CompareStringAndBuffer {
//			@Test
//			public void compareStringAndStringBuffer() {
				String a="a";
				String b="a";
				String c= new String("a");
				String d= new String("a");
				StringBuilder sb = new StringBuilder("a");
				StringBuilder sb1 = new StringBuilder("a");
				if(a.equals(b))
					System.out.println("a is equal to b");   //a.equals(b)?a:b;
				else
					System.out.println("a is not equal to b");
				if(a == b)
					System.out.println("a is equal to b");
				else
					System.out.println("a is not equal to b");
				if(c == d)
					System.out.println("c is equal to d");
				else
					System.out.println("c is not equal to d");
				if(c.equals(d))
					System.out.println("c is equal to d");
				else
					System.out.println("c is not equal to d");
				if(a.equals(c))
					System.out.println("a is equal to c");
				else
					System.out.println("a is not equal to c");
				if(sb.equals(sb1))
					System.out.println("sb is equal to sb1");
				else
					System.out.println("sb is not equal to sb1");
				if(sb == sb1)
					System.out.println("sb is equal to sb1");
				else
					System.out.println("sb is not equal to sb1");
				/*
				 *we cannot compare string and string builder directly, 
				 *we have to convert string builder object to string 
				*/  
				if(sb.toString().equals(c))
					System.out.println("a is equal to b");
				else
					System.out.println("a is not equal to b");

				
		
	}

}
