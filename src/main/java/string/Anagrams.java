package string;

import java.util.Arrays;

public class Anagrams {

	public static void main(String[] args) {
		String str1="listen";
		String str2="silent";
		boolean status = true;  
		int length1 = str1.length();
		int length2 = str2.length();
		if(length1!=length2) {

			status = false;
		}
		else
		{	
			char[] ch1 = str1.toLowerCase().toCharArray();
			char[] ch2 = str2.toLowerCase().toCharArray();
			Arrays.sort(ch1);
			Arrays.sort(ch2);
			status=Arrays.equals(ch1, ch2);
		}
		if(status) {
			System.out.println("It is anagram");
		}
		else 
		{
			System.out.println("It is not anagram");
		}

	}
}
