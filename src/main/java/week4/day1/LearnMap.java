package week4.day1;

import java.util.HashMap;

import java.util.Map;
import java.util.Map.Entry;

public class LearnMap {

	public static void main(String[] args) {
		
		String text="testleeaf";
		char[] ch=text.toCharArray();
		
		Map<Character,Integer> map=new HashMap<>();
		for (char c : ch) {
			
			int value=1;
			
			if(map.containsKey(c)) {
				value=value+1;
				map.put(c, map.get(c)+1);			
			}
			else
			{
				map.put(c, value);				
			}
		}
			for(Entry<Character,Integer> each:map.entrySet()) {
				System.out.println(each.getKey()+"-->"+each.getValue());
			}

	}

}
