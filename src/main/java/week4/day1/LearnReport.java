package week4.day1;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class LearnReport {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//Path of report
		ExtentHtmlReporter html= new ExtentHtmlReporter("./reports/result.html");
		html.setAppendExisting(true);
		ExtentReports extent=new ExtentReports();
		//attach report
		extent.attachReporter(html);
		ExtentTest test = extent.createTest("Tc001_LoginAndLogout", "Login to Leaftaps");
		test.assignAuthor("Suryalekha");
		test.assignCategory("smoke");
		//syso
		test.pass("Entered Username successfully");
		test.fail("Entered Password successfully");
		test.fail("Login to leaftap not successfull");
		//generate report
		extent.flush();
		
	}

}
