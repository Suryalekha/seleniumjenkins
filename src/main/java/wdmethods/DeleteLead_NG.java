package wdmethods;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

public class DeleteLead_NG extends ProjectSpecificMethods{

	@Test/*(groups="reg",dependsOnGroups="smoke")*/
	public void deleteLead() throws InterruptedException {
		//Click on Leads link
		WebElement eleLeadsLink = locateElement("LinkText","Leads");
		click(eleLeadsLink);
		//Click on Find Leads link
		WebElement eleFindLeadsLink = locateElement("LinkText","Find Leads");
		click(eleFindLeadsLink);
		//Click on Phone
		WebElement elePhone = locateElement("XPath","//span[text()='Phone']");
		click(elePhone);
		WebElement elePhAreaCode = locateElement("name", "phoneAreaCode");
		type(elePhAreaCode, "044");
		WebElement elePhNum = locateElement("name", "phoneNumber");
		type(elePhNum, "26565158");
		//Click on Find Leads button
		WebElement eleFindLeadsBtn = locateElement("XPath","//button[text()='Find Leads']");
		click(eleFindLeadsBtn);
		Thread.sleep(4000);
		//Capture First Lead ID
		WebElement eleFirstIDLink = locateElement("XPath","(//a[@class='linktext'])[4]");
		String textFirstLink= getText(eleFirstIDLink);
		System.out.println(textFirstLink);
		//Click First Lead ID
		click(eleFirstIDLink);
		//Click Delete
		WebElement eleDelBtn = locateElement("class","subMenuButtonDangerous");
		click(eleDelBtn);
		Thread.sleep(2000);
		//Click on Find Leads link
		WebElement eleFindLeadLink = locateElement("LinkText","Find Leads");
		click(eleFindLeadLink);
		Thread.sleep(4000);
		WebElement eleLeadID = locateElement("name", "id");
//		WebElement eleLeadID = locateElement("XPath", "(//input[@style='width: 212px;'])[1]");
		type(eleLeadID, textFirstLink);
		Thread.sleep(4000);
		//Click on Find Leads button
		WebElement eleFindLeadBtn = locateElement("XPath","//button[text()='Find Leads']");
		click(eleFindLeadBtn);
		Thread.sleep(4000);
		WebElement eleErrMsg = locateElement("XPath","//div[text()='No records to display']");
		System.out.println(eleErrMsg);
		//		String textError= getText(eleErrMsg);
		//verifyPartialText(eleErrMsg,"No records to display");
		System.out.println("Lead deleted successfully.");
	}

}
