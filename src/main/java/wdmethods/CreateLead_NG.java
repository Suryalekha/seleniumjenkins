package wdmethods;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import week3.day3.ReadExcel;

public class CreateLead_NG extends ProjectSpecificMethods{
	
	@Test(dataProvider="fetchData")
	public void createLead(String cname,String fname,String lname,String PhAreaCode,String PhoneNo,String email) {
	
	WebElement eleLeadsLink = locateElement("LinkText","Leads");
	click(eleLeadsLink);
	WebElement eleCreateLeadLink = locateElement("LinkText","Create Lead");
	click(eleCreateLeadLink);
	
	//Create Lead Page
	WebElement eleCompName = locateElement("id", "createLeadForm_companyName");
	type(eleCompName, cname);
	WebElement eleFirstName = locateElement("id", "createLeadForm_firstName");
	type(eleFirstName, fname);	
	WebElement eleLastName = locateElement("id", "createLeadForm_lastName");
	type(eleLastName, lname);
	
	WebElement eleAreaCode= locateElement("id", "createLeadForm_primaryPhoneAreaCode");
	type(eleAreaCode, PhAreaCode);
	WebElement elePhNo= locateElement("id", "createLeadForm_primaryPhoneNumber");
	type(elePhNo,PhoneNo);
	/*WebElement eleSource = locateElement("id", "createLeadForm_dataSourceId");
	selectDropDownUsingText(eleSource,"Conference");
	WebElement eleMarkCamp = locateElement("id", "createLeadForm_marketingCampaignId");
	selectDropDownUsingText(eleMarkCamp,"Automobile");
	WebElement eleFNameLoc = locateElement("id", "createLeadForm_firstNameLocal");
	type(eleFNameLoc, "Dk");
	WebElement eleLNameLoc = locateElement("id", "createLeadForm_lastNameLocal");
	type(eleLNameLoc, "RRR");
	WebElement eleSalu = locateElement("id", "createLeadForm_personalTitle");
	type(eleSalu, "Hi");
	WebElement eleTitle = locateElement("id", "createLeadForm_generalProfTitle");
	type(eleTitle, "Miss");
	WebElement eleAnnRev = locateElement("id", "createLeadForm_annualRevenue");
	type(eleAnnRev, "500000");
	WebElement eleDepName= locateElement("id", "createLeadForm_departmentName");
	type(eleDepName, "Treasury");
	WebElement eleCurr = locateElement("id", "createLeadForm_currencyUomId");
	selectDropDownUsingText(eleCurr,"INR - Indian Rupee");
	WebElement eleSicCode= locateElement("id", "createLeadForm_sicCode");
	type(eleSicCode, "GGGGGG");
	WebElement eleDesc= locateElement("id", "createLeadForm_description");
	type(eleDesc, "JJJJJJJJJJ");
	WebElement eleImpNote= locateElement("id", "createLeadForm_importantNote");
	type(eleImpNote, "QQQQQQQQQQQQ");
	WebElement eleTickSym= locateElement("id", "createLeadForm_tickerSymbol");
	type(eleTickSym, "aaaaaaaaaaa");
	WebElement eleNoEmp = locateElement("id", "createLeadForm_numberEmployees");
	type(eleNoEmp, "2000");
	WebElement eleInds = locateElement("id", "createLeadForm_industryEnumId");
	selectDropDownUsingText(eleInds,"Finance");
	WebElement eleOwner = locateElement("id", "createLeadForm_ownershipEnumId");
	selectDropDownUsingText(eleOwner,"Partnership");*/
	
	//Contact Info
	
	WebElement eleEmail= locateElement("id", "createLeadForm_primaryEmail");
	type(eleEmail, email);
	/*WebElement eleAskName= locateElement("id", "createLeadForm_primaryPhoneAskForName");
	type(eleAskName, "Abi");
	WebElement elePhExt= locateElement("id", "createLeadForm_primaryPhoneExtension");
	type(elePhExt, "13456");
	WebElement eleURL= locateElement("id", "createLeadForm_primaryWebUrl");
	type(eleURL, "http://url.com/");
	
	WebElement eleToName= locateElement("id", "createLeadForm_generalToName");
	type(eleToName, "Dora");
	WebElement eleAttnName= locateElement("id", "createLeadForm_generalAttnName");
	type(eleAttnName, "kutty");
	WebElement eleAddr1= locateElement("id", "createLeadForm_generalAddress1");
	type(eleAddr1, "23,kannan street");
	WebElement eleAddr2= locateElement("id", "createLeadForm_generalAddress2");
	type(eleAddr2, "Avadi");
	WebElement eleCity= locateElement("id", "createLeadForm_generalCity");
	type(eleCity, "Chennai");
	WebElement eleState= locateElement("id", "createLeadForm_generalStateProvinceGeoId");
	type(eleState, "TAMILNADU");
	WebElement eleCtry= locateElement("id", "createLeadForm_generalCountryGeoId");
	type(eleCtry, "India");
	WebElement elePostCode= locateElement("id", "createLeadForm_generalPostalCode");
	type(elePostCode, "600129");
	
	WebElement eleSubmit = locateElement("class","smallSubmit");
	click(eleSubmit);
	WebElement eleFname= locateElement("id","viewLead_firstName_sp");
	verifyExactText(eleFname,"Daksha");*/
	
	WebElement eleSubmit = locateElement("class","smallSubmit");
	click(eleSubmit);
	}
	
	@DataProvider(name="fetchData")
	public String[][] dynamicData(){
		String[][] data=new String[2][6];
		
		data[0][0]="Tata";
		data[0][1]="Daksha";
		data[0][2]="Rupa";
		data[0][3]="044";
		data[0][4]="26565158";
		data[0][5]="DakshaRupa@gmail.com";
		
		data[1][0]="Tata";
		data[1][1]="Daksha";
		data[1][2]="Rupa";
		data[1][3]="044";
		data[1][4]="26565158";
		data[1][5]="DakshaRupa@gmail.com";
		
		return data;
	}
	
	
}
