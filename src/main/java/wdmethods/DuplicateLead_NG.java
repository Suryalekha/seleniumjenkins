package wdmethods;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

public class DuplicateLead_NG  extends ProjectSpecificMethods{

	@Test
	public void duplicateLead() throws InterruptedException {
		//Click on Leads link
		WebElement eleLeadsLink = locateElement("LinkText","Leads");
		click(eleLeadsLink);
		//Click on Find Leads link
		WebElement eleFindLeadsLink = locateElement("LinkText","Find Leads");
		click(eleFindLeadsLink);
//		Click on email
		click(locateElement("XPath","//span[text()='Email']"));
		WebElement eleEmail = locateElement("name", "emailAddress");
		type(eleEmail, "DakshaRupa@gmail.com");
//		Click on Find Leads button
		WebElement eleFindLeadsBtn = locateElement("XPath","//button[text()='Find Leads']");
		click(eleFindLeadsBtn);
		Thread.sleep(3000);
//		Capture First name of First Resulting lead
		String fName = getText(locateElement("XPath", "(//a[@class='linktext'])[6]"));
		System.out.println(fName);
		WebElement eleFirstIDLink = locateElement("XPath","(//a[@class='linktext'])[4]");
		click(eleFirstIDLink);
		//Click Duplicate Lead
		WebElement eleDupBtn = locateElement("LinkText","Duplicate Lead");
		click(eleDupBtn);
//		Verify the title as 'Duplicate Lead'
		WebElement eleTitle= locateElement("id","sectionHeaderTitle_leads");
		verifyExactText(eleTitle,"Duplicate Lead");
		System.out.println("Title is:"+eleTitle);
		WebElement eleSubmit = locateElement("class","smallSubmit");
		click(eleSubmit);
		if(fName.equals("Daksha")) {
			System.out.println("Duplicated lead name is same as captured name");
		}
		else
		{
			System.out.println("Duplicated lead name is not same as captured name");
		}
	}

}
