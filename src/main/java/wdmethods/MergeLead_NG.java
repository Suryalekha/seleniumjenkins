package wdmethods;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

public class MergeLead_NG extends ProjectSpecificMethods{

	@Test
	public void mergeLead() throws InterruptedException {
		//Click on Leads link
		WebElement eleLeadsLink = locateElement("LinkText","Leads");
		click(eleLeadsLink);
		//Click Merge Leads
		WebElement eleMergeLead = locateElement("LinkText","Merge Leads");
		click(eleMergeLead);

		WebElement eleFromIcon = locateElement("XPath","(//img[@src='/images/fieldlookup.gif'])[1]");
		click(eleFromIcon);
		switchToWindow(1);
		type(locateElement("name","firstName"), "Daksha");
		WebElement eleFLead = locateElement("XPath","(//button[@class='x-btn-text'])[1]");
		click(eleFLead);
		Thread.sleep(3000);
		String text = getText(locateElement("XPath", "(//a[@class='linktext'])[1]"));
//		System.out.println(text);
		click(locateElement("XPath","(//a[@class='linktext'])[1]"));
		switchToWindow(0);

		WebElement eleToIcon = locateElement("XPath","(//img[@src='/images/fieldlookup.gif'])[2]");
		click(eleToIcon);
		switchToWindow(1);
//		clear(locateElement("name","firstName"));
		WebElement eleFName=locateElement("name","firstName");
		type(eleFName, "Raj");
		Thread.sleep(5000);
		WebElement eleFLead1 = locateElement("XPath","(//button[@class='x-btn-text'])[1]");
		click(eleFLead1);
		Thread.sleep(5000);
		String text1 = getText(locateElement("XPath", "(//a[@class='linktext'])[1]"));
		System.out.println(text1);
		click(locateElement("XPath","(//a[@class='linktext'])[1]"));
		switchToWindow(0);
		click(locateElement("LinkText","Merge"));
		acceptAlert();

		WebElement eleFindLeadsLink = locateElement("LinkText","Find Leads");
		click(eleFindLeadsLink);
		WebElement eleN = locateElement("XPath", "(//input[@style='width: 212px;'])[1]");
		type(eleN, text);
		click(locateElement("XPath","(//button[@class='x-btn-text'])[7]"));
		Thread.sleep(3000);
		String errMsg = getText(locateElement("class", "x-paging-info"));
		System.out.println(errMsg);
	}

}
