package wdmethods;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

public class EditLead_NG extends ProjectSpecificMethods{

	@Test/*(groups="sanity",dependsOnGroups="smoke")*/
	public void editLead() throws InterruptedException {
		//Click on Leads link
		WebElement eleLeadsLink = locateElement("LinkText","Leads");
		click(eleLeadsLink);
		//Click on Find Leads link
		WebElement eleFindLeadsLink = locateElement("LinkText","Find Leads");
		click(eleFindLeadsLink);
		WebElement eleFName = locateElement("XPath", "(//input[@name='firstName'])[3]");
		type(eleFName, "Daksha");
		//Click on Find Leads button
		WebElement eleFindLeadsBtn = locateElement("XPath","//button[text()='Find Leads']");
		click(eleFindLeadsBtn);
		//webdriver wait
		/*WebDriverWait wait = new WebDriverWait(driver,30);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//a[@class='linktext'])[4]")));*/
		Thread.sleep(3000);

		WebElement eleFirstIDLink = locateElement("XPath","(//a[@class='linktext'])[4]");
		click(eleFirstIDLink);
		verifyTitle("View Lead | opentaps CRM");
		WebElement eleEditBtn = locateElement("XPath","(//a[@class='subMenuButton'])[3]");
		click(eleEditBtn);
		WebElement eleCmpName = locateElement("XPath","(//input[@name='companyName'])[2]");
		clear(eleCmpName);
		type(eleCmpName,"TestLeaf");
		WebElement eleUpdatBtn = locateElement("XPath","(//input[@class='smallSubmit'])[1]");
		click(eleUpdatBtn);
		WebElement eleCname = locateElement("id","viewLead_companyName_sp");
		verifyExactText(eleCname, "TestLeaf");
		System.out.println("Company name changed.");

	}
}
