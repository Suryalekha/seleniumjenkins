package wdmethods;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;

public class ProjectSpecificMethods extends SeMethods{
	//	public ChromeDriver driver;

	@BeforeMethod/*(groups="common")*/
	@Parameters({"url","browser","username","password"})
	public void login(String url,String browser,String uname,String pswd ) {
		/*driver= new ChromeDriver();
		//Load URL
		driver.get("http:leaftaps.com/opentaps/");
		//maximize
		driver.manage().window().maximize();*/

		startApp(browser,url);
		//Enter username
		WebElement eleUsername = locateElement("id", "username");
		type(eleUsername, uname);
		//Enter Password
		WebElement elePassword = locateElement("id", "password");
		type(elePassword, pswd);
		//Login
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin);
		//Click on CRMSFA link
		WebElement eleCRMLink = locateElement("LinkText","CRM/SFA");
		click(eleCRMLink);
	}
	
	@AfterMethod/*(groups="common")*/
	public void close() {
		closeAllBrowsers();
	}
}
