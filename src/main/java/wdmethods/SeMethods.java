package wdmethods;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;

public class SeMethods implements WdMethods {

	RemoteWebDriver driver = null;
	public static int i;
	
	@Override
	public void startApp(String browser, String url) {
	
	if (browser.equalsIgnoreCase("chrome")) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		driver = new ChromeDriver();
	} else if(browser.equalsIgnoreCase("firefox")) {
		System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver.exe");
		driver = new FirefoxDriver();
	}
	driver.get(url);
	driver.manage().window().maximize();
	System.out.println("The browser "+browser+" launched successfully");
	}

	@Override
	public WebElement locateElement(String locator, String locValue) {
		try {
			switch (locator) {
			case "LinkText": return driver.findElementByLinkText(locValue);
			case "id": return driver.findElementById(locValue); 
			case "class": return  driver.findElementByClassName(locValue);
			case "XPath": return  driver.findElementByXPath(locValue);
			case "name": return  driver.findElementByName(locValue);
			case "tagName": return  driver.findElementByTagName(locValue);
			}
		} catch (NoSuchElementException e) {
			System.err.println("NoSuchElementException occured");
		}catch (WebDriverException e) {
			System.err.println("WebDriverException occured");
		}
		return null;
	}

	@Override
	public WebElement locateElement(String locValue) {
		// TODO Auto-generated method stub
		return driver.findElementById(locValue);
	}

	@Override
	public void type(WebElement ele, String data) {
       ele.sendKeys(data);	
       System.out.println("The data "+data+" entered successfully");
	}

	@Override
	public void click(WebElement ele) {
		ele.click();
		System.out.println("The element "+ele+ "click successfully");
	}

	@Override
	public String getText(WebElement ele) {
		// TODO Auto-generated method stub
		String text = ele.getText();
		return text;
	}
	
	@Override
	public void clear(WebElement ele) {
		// TODO Auto-generated method stub
		ele.clear();		
	}

	@Override
	public void selectDropDownUsingText(WebElement ele, String value) {
	Select sel = new Select(ele);
	sel.selectByVisibleText(value);
	System.out.println("The value "+value+" entered successfully");
	}

	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) {
		// TODO Auto-generated method stub
		Select sel = new Select(ele);
		sel.selectByIndex(index);
	}

	@Override
	public boolean verifyTitle(String expectedTitle) {
		// TODO Auto-generated method stub
		String title = driver.getTitle();
		if(title.equalsIgnoreCase(expectedTitle)) {
			System.out.println("Title verified");
			return true;
		}
		else {	
		return false;
		}
	}

	@Override
	public void verifyExactText(WebElement ele, String expectedText) {
		// TODO Auto-generated method stub
		String text = ele.getText();
		if(text.equalsIgnoreCase(expectedText)) {
			System.out.println("Text matches exactly");		
		}
		else {
			System.out.println("Text does not match");
		}
	
	}

	@Override
	public void verifyPartialText(WebElement ele, String expectedText) {
		// TODO Auto-generated method stub
		String text = ele.getText();
		if(text.contains(expectedText)) {
			System.out.println("Text matches partially");		
		}
		else {
			System.out.println("Text does not match");
		}
	}

	@Override
	public void verifyExactAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub
		String expectedValue = ele.getAttribute(attribute);
	if(expectedValue.equalsIgnoreCase(value)) {
			System.out.println("Attribute matches exactly");
		}
	}

	@Override
	public void verifyPartialAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub
		String expectedValue = ele.getAttribute(attribute);
		if(expectedValue.contains(value)) {
				System.out.println("Attribute matches partially");
			}
	}

	@Override
	public void verifySelected(WebElement ele) {
		// TODO Auto-generated method stub
		ele.isSelected();
		System.out.println("The element is selected successfully");
	}

	@Override
	public void verifyDisplayed(WebElement ele) {
		// TODO Auto-generated method stub
		ele.isDisplayed();
		System.out.println("The element is displayed successfully");
	}

	@Override
	public void switchToWindow(int index) {
		// TODO Auto-generated method stub
		Set<String> allWindows = driver.getWindowHandles();
		List<String> list=new ArrayList<String>();
		list.addAll(allWindows);
		driver.switchTo().window(list.get(index));		
	}

	@Override
	public void switchToFrame(WebElement ele) {
		// TODO Auto-generated method stub
		driver.switchTo().frame(ele);
	}

	@Override
	public void acceptAlert() {
		// TODO Auto-generated method stub
		driver.switchTo().alert().accept();
	}

	@Override
	public void dismissAlert() {
		// TODO Auto-generated method stub
		driver.switchTo().alert().dismiss();
	}

	@Override
	public String getAlertText() {
		// TODO Auto-generated method stub
		String text = driver.switchTo().alert().getText();
		return text;
	}

	@Override
	public void takeSnap() {
		// TODO Auto-generated method stub
		File src = driver.getScreenshotAs(OutputType.FILE);
		File dest = new File("./snaps/img"+i+".png");
		try {
			FileUtils.copyFile(src, dest);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		i++;
	}

	@Override
	public void closeBrowser() {
		// TODO Auto-generated method stub
		driver.close();
	}

	@Override
	public void closeAllBrowsers() {
		// TODO Auto-generated method stub
		driver.quit();
		
	}

}
