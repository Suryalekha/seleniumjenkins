package week3.day3;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import wdmethods.ProjectSpecificMethods;

public class EditLead_P extends ProjectSpecificMethods{
	
	@Test(dataProvider="EditLead")
	public void editLead(String fname,String cname) {
		
		//Click on Leads link
				WebElement eleLeadsLink = locateElement("LinkText","Leads");
				click(eleLeadsLink);
				//Click on Find Leads link
				WebElement eleFindLeadsLink = locateElement("LinkText","Find Leads");
				click(eleFindLeadsLink);
				WebElement eleFName = locateElement("XPath", "(//input[@name='firstName'])[3]");
				type(eleFName, fname);
				//Click on Find Leads button
				WebElement eleFindLeadsBtn = locateElement("XPath","//button[text()='Find Leads']");
				click(eleFindLeadsBtn);
				//webdriver wait
				/*WebDriverWait wait = new WebDriverWait(driver,30);
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//a[@class='linktext'])[4]")));*/
				try {
					Thread.sleep(3000);
				} catch (InterruptedException e) {
					System.err.println("Interrupted Exception occured");
					
				}

				WebElement eleFirstIDLink = locateElement("XPath","(//a[@class='linktext'])[4]");
				click(eleFirstIDLink);
				verifyTitle("View Lead | opentaps CRM");
				WebElement eleEditBtn = locateElement("XPath","(//a[@class='subMenuButton'])[3]");
				click(eleEditBtn);
				WebElement eleCmpName = locateElement("XPath","(//input[@name='companyName'])[2]");
				clear(eleCmpName);
				type(eleCmpName,cname);
				WebElement eleUpdatBtn = locateElement("XPath","(//input[@class='smallSubmit'])[1]");
				click(eleUpdatBtn);
				WebElement eleCname = locateElement("id","viewLead_companyName_sp");
				verifyExactText(eleCname, cname);
				System.out.println("Company name changed.");

	}
	@DataProvider(name="EditLead")
	public String[][] getData() throws IOException{
		String[][] data = ReadExcel.readExcel("EditLead");
		return data;
	}	

}
