package week3.day3;
import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import wdmethods.ProjectSpecificMethods;


public class CreateLead_P extends ProjectSpecificMethods{
	
	@Test(dataProvider="CreateLead")
	public void createLead(String cname,String fname,String lname,String email,String PhAreaCode,String PhoneNo,String city,String state) {
	
	WebElement eleLeadsLink = locateElement("LinkText","Leads");
	click(eleLeadsLink);
	WebElement eleCreateLeadLink = locateElement("LinkText","Create Lead");
	click(eleCreateLeadLink);
	
	//Create Lead Page
	WebElement eleCompName = locateElement("id", "createLeadForm_companyName");
	type(eleCompName, cname);
	WebElement eleFirstName = locateElement("id", "createLeadForm_firstName");
	type(eleFirstName, fname);	
	WebElement eleLastName = locateElement("id", "createLeadForm_lastName");
	type(eleLastName, lname);
	WebElement eleEmail= locateElement("id", "createLeadForm_primaryEmail");
	type(eleEmail, email);
	WebElement eleAreaCode= locateElement("id", "createLeadForm_primaryPhoneAreaCode");
	type(eleAreaCode,PhAreaCode);
	WebElement elePhNo= locateElement("id", "createLeadForm_primaryPhoneNumber");
	type(elePhNo, PhoneNo);
	WebElement eleCity= locateElement("id", "createLeadForm_generalCity");
	type(eleCity, city);
	WebElement eleState= locateElement("id", "createLeadForm_generalStateProvinceGeoId");
	type(eleState, state);
	WebElement eleSubmit = locateElement("class","smallSubmit");
	click(eleSubmit);
	}
	@DataProvider(name="CreateLead")
	public String[][] getData() throws IOException{
		String[][] data = ReadExcel.readExcel("CreateLead");
		return data;
	}	
}
