package week3.day3;

import java.io.IOException;

import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadExcel {
	public static String[][] readExcel(String fileName) throws IOException{
		//Open Workbook
		XSSFWorkbook wbook=new XSSFWorkbook("./data/"+fileName+".xlsx");
		//goto sheet
		XSSFSheet sheet = wbook.getSheetAt(0);
		//goto row
		int rowCount = sheet.getLastRowNum();
		System.out.println(rowCount);
		//Column
		int colCount = sheet.getRow(0).getLastCellNum();
		System.out.println(colCount);
		
		String[][] data=new String[rowCount][colCount];
		
		for (int i = 1; i <= rowCount; i++) {
			XSSFRow row = sheet.getRow(i);
			for (int j = 0; j < colCount; j++) {
				//go to column of row
			XSSFCell column = row.getCell(j);
			
			String cellValue = "";
			if(column.getCellTypeEnum() == CellType.NUMERIC)
				cellValue = ""+column.getNumericCellValue();
			else if(column.getCellTypeEnum() == CellType.STRING)
				cellValue = column.getStringCellValue();
			else if(column.getCellTypeEnum() == CellType.FORMULA)
				cellValue = column.getCellFormula();
			
			//read data
			data[i-1][j] = cellValue;
			}
		}
		return data;
	}

}
