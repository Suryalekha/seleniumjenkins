package week3.day1;

import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import wdmethods.ProjectSpecificMethods;

public class DeleteLead extends ProjectSpecificMethods {
	public ChromeDriver driver;
	@Test()
	public void deleteLead() throws InterruptedException {
		driver= new ChromeDriver();
		//Load URL
		driver.get("http:leaftaps.com/opentaps/");
		//maximize
		driver.manage().window().maximize();
		//Enter username
		driver.findElementById("username").sendKeys("DemoSalesManager");
		//password
		driver.findElementById("password").sendKeys("crmsfa");
		//Login
		driver.findElementByClassName("decorativeSubmit").click();
		//Click on CRMSFA link
		driver.findElementByLinkText("CRM/SFA").click();
		//Click on Leads link
		driver.findElementByLinkText("Leads").click();
		//Click on Find Leads link
		driver.findElementByLinkText("Find Leads").click();
		//Click on Phone
		driver.findElementByXPath("//span[text()='Phone']").click();
		driver.findElementByName("phoneAreaCode").sendKeys("044");
		driver.findElementByName("phoneNumber").sendKeys("26565158");
		//Click on Find Leads button
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(3000);
		//Capture lead ID of First Resulting lead
		String text = driver.findElementByXPath("(//a[@class='linktext'])[4]").getText();	
		System.out.println(text);
		//Click on first resulting lead id
		driver.findElementByXPath("(//a[@class='linktext'])[4]").click();
		//Click Delete
		driver.findElementByClassName("subMenuButtonDangerous").click();
		driver.findElementByLinkText("Find Leads").click();
		driver.findElementByXPath("(//input[@style='width: 212px;'])[1]").sendKeys(text);
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(3000);
		String text2 = driver.findElementByXPath("//div[text()='No records to display']").getText();
		if(text2.equals("No records to display")) {
			System.out.println("Lead deleted successfully.");
		}
		else {
			System.out.println("Lead not deleted.");
		}
		driver.close();		
	}
}

