package regexp;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.support.ui.Wait;

public class RegExp {

	public static void main(String[] args) {
		
	
		/*String data="4567 8765 3421 4567";
		String pat="\\d{4}\\s\\d{4}\\s\\d{4}\\s\\d{4}";
		Pattern compile = Pattern.compile(pat);
		Matcher matcher = compile.matcher(data);	
		System.out.println (matcher.matches());*/
		
		
		String email="abcd@gmail.com";
		String pat="[a-zA-Z]+\\d*\\W[a-z]+\\W[a-z]+";
		Pattern compile = Pattern.compile(pat);
		Matcher matcher = compile.matcher(email);	
		System.out.println (matcher.matches());
	}

}
