package seleniumInterview;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.LocalFileDetector;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.RemoteWebElement;
import org.openqa.selenium.support.ui.Select;

public class UploadFile {

	public static void main(String[] args) {
		RemoteWebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
//		driver.navigate().to("https://www.google.co.in");
		driver.get("https://www.naukri.com/");
		WebElement ele = driver.findElementByXPath("//input[@type='file']");
		LocalFileDetector detector = new LocalFileDetector();
		((RemoteWebElement)ele).setFileDetector(detector);
		ele.sendKeys(detector.getLocalFile("./Resume.doc").getAbsolutePath());
		
		/*WebElement selectCountry = driver.findElementById("createLeadForm_generalCountryGeoId");
		Select select = new Select(selectCountry);
		List<WebElement> options = select.getOptions();
		options.size();
		for (WebElement webElement : options) {
			 if(webElement.getText().startsWith("E")) {
				 int indexOf = options.indexOf(webElement);
				 select.selectByIndex(indexOf+1);
				 System.out.println(select.getFirstSelectedOption().getText());
			 }
		}*/
	}

}
