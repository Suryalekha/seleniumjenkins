package Actions;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.Keys;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;

public class DragAndDrop {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		RemoteWebDriver driver= new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://jqueryui.com/");
		driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
		driver.findElementByLinkText("Draggable").click();
		WebElement frameElmnt = driver.findElementByClassName("demo-frame");
		driver.switchTo().frame(frameElmnt);
		WebElement drag = driver.findElementById("draggable");
		/*int xOffset= frameElmnt.getLocation().getX();
		int yOffset= frameElmnt.getLocation().getY();
		System.out.println(xOffset+" "+yOffset);*/
		Actions builder=new Actions(driver);
		builder.dragAndDropBy(drag,100, 100).perform();
		System.out.println("Drag and Drop performed");
		
		//Selectable
		/*driver.switchTo().defaultContent();
		
		driver.findElementByLinkText("Selectable").click();
		driver.switchTo().frame(0);
		
		WebElement ele1 = driver.findElementByXPath("//li[text()='Item 1']");
		WebElement ele2 = driver.findElementByXPath("//li[text()='Item 4']");
		WebElement ele3 = driver.findElementByXPath("//li[text()='Item 7']");
		
		
		builder.sendKeys(Keys.CONTROL);
		builder.click(ele1).perform();
		builder.click(ele2).perform();
		builder.click(ele3).perform();*/
		
		//Sortable
		driver.switchTo().defaultContent();
		driver.findElementByLinkText("Sortable").click();
		driver.switchTo().frame(0);
		
		WebElement item1 = driver.findElementByXPath("//li[text()='Item 1']");
		WebElement item2 = driver.findElementByXPath("//li[text()='Item 2']");
		WebElement item3 = driver.findElementByXPath("//li[text()='Item 3']");
		WebElement item4 = driver.findElementByXPath("//li[text()='Item 4']");
		WebElement item5 = driver.findElementByXPath("//li[text()='Item 5']");
		WebElement item6 = driver.findElementByXPath("//li[text()='Item 6']");
		WebElement item7 = driver.findElementByXPath("//li[text()='Item 7']");
		
		int y = item5.getLocation().getY();
		int x = item5.getLocation().getX();
		System.out.println(x+" "+y);
		
		builder.dragAndDrop(item1, item2).perform();
		builder.dragAndDrop(item2, item1).perform();
		builder.dragAndDrop(item3, item4).perform();
		builder.dragAndDrop(item4, item3).perform();
		builder.dragAndDrop(item5, item6).perform();
		builder.dragAndDrop(item6, item5).perform();
		builder.dragAndDrop(item5, item7).perform();
		builder.dragAndDrop(item7, item5).perform();
		
//		builder.dragAndDropBy(item2, x, y).click().perform();
		
//		driver.close();
	}

}
