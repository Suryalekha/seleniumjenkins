/*package steps;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver.Options;
import org.openqa.selenium.WebDriver.Window;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


public class CreateLead {

	public ChromeDriver driver;

	@Given("Launch browser")
	public void launchBrowser() {
		driver= new ChromeDriver();
	}
	@And("Load URL")
	public void loadURL() {
		driver.get("http:leaftaps.com/opentaps/");
	}

	@When("Maximize browser")
	public void maximize() {
		Options manage = driver.manage();window().maximize();
		Window window = manage.window();
		window.maximize();
	}
	@And("Set the timeout")
	public void timeout() {
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}
	@When("Enter username")
	public void typeUsername() {
		driver.findElementById("username").sendKeys("DemoSalesManager");
	}
	@And("Enter password")
	public void typePassword() {
		driver.findElementById("password").sendKeys("crmsfa");
	}
	@When("Click Login")
	public void clickLogin() {
		driver.findElementByClassName("decorativeSubmit").click();
	}
	@Then("Verify login")
	public void verify() {
		System.out.println("Login verified");
	}

	@And("Goto crmsfa Page")
	public void clickCRM() {
		driver.findElementByLinkText("CRM/SFA").click();
	}

	@And("Click Leads link")
	public void clickLeads() {
		driver.findElementByLinkText("Leads").click();
	}

	@And("Click Create lead link")
	public void clickCreateLead() {
		driver.findElementByLinkText("Create Lead").click();
	}

	@When("Enter First name as(.*)")
	public void typeFirstName(String fname) {
		driver.findElementById("createLfnameeadForm_firstName").sendKeys(fname);
	}

	@And("Enter Last name as (.*)")
	public void typeLastName(String lname) {
		driver.findElementById("createLeadForm_lastName").sendKeys(lname);
	}

	@And("Enter Company name as(.*)")
	public void typeCompName(String cnmae) {
		driver.findElementById("createLeadForm_companyName").sendKeys(cnmae);
	}

	@And("Enter Email id as(.*)")
	public void typeEmail(String email) {
		driver.findElementById("createLeadForm_primaryEmail").sendKeys(email);
	}
	@When("Click Submit button")
	public void clickSubmit() {
		driver.findElementByClassName("smallSubmit").click();
	}

	@Then("Verify first name in View leads")
	public void verifyCreate() {
		String text = driver.findElementById("viewLead_firstName_sp").getText();
		if(text.equals("Daksha")) {
			System.out.println("First name is verified.Lead created.");
		}
		else if(text.equals("Raj")) {
			System.out.println("First name is verified.Lead created.");
		}
		else
		{
			throw new RuntimeException();
		}
	}
	
	@And("Close Browser")
	public void closeBrowser() {
		driver.close();
	}
}
*/